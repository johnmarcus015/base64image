package com.converter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @version 1.0
 * @since 11-07-2016
 * @author johnmarcus015
 * @see https://bitbucket.org/johnmarcus015/
 * @see https://commons.apache.org/proper/commons-codec/apidocs/org/apache/commons/codec/binary/Base64.html
 */
public class Base64 {

    /**
     * 
     * @param pathOfImage
     * @return 
     */
    public String encodeImage(String pathOfImage) {
        FileInputStream input = null;
        String base64 = null;
        try {
            File file = new File(pathOfImage);
            input = new FileInputStream(file);
            byte imageBytes[] = new byte[(int) file.length()];
            input.read(imageBytes);
            base64 = org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(imageBytes);
        } catch (FileNotFoundException ex) {
            //ERROR: Image not found!
            Logger.getLogger(Base64.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Base64.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                input.close();
            } catch (IOException ex) {
                Logger.getLogger(Base64.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return base64;
    }

    /**
     * 
     * @param stringBase64
     * @param pathOfImage 
     */
    public void decodeImage(String stringBase64, String pathOfImage) {
        try {
            byte imageBytes[] = org.apache.commons.codec.binary.Base64.decodeBase64(stringBase64);
            FileOutputStream output = new FileOutputStream(pathOfImage);
            output.write(imageBytes);
            output.close();
        } catch (FileNotFoundException ex) {
            //ERROR: Image not found!!!
            Logger.getLogger(Base64.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            //ERROR: Can't read Image!!!
            Logger.getLogger(Base64.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
