package com.examples;

import com.converter.Base64;

/**
 *
 * @version 1.0
 * @since 11-07-2016
 * @author johnmarcus015
 * @see https://bitbucket.org/johnmarcus015/
 */
public class ImageToBase64 {

    public static void main(String[] args) {

        String path = "C:\\Users\\MONSTHER\\Pictures\\";
        
        String base64S = new Base64().encodeImage(path+"Imagem1.png");
        System.out.println(base64S);    
    }
}
