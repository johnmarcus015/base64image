package com.examples;

import com.converter.Base64;

/**
 *
 * @version 1.0
 * @since 11-07-2016
 * @author johnmarcus015
 * @see https://bitbucket.org/johnmarcus015/
 */
public class Base64ToImage {

    public static void main(String args[]) {
        
        String path = "C:\\Users\\MONSTHER\\Pictures\\";

        Base64 base64 = new Base64();
        String base64S = base64.encodeImage(path + "Imagem1.png");
        base64.decodeImage(base64S, path + "Image4.png");
    }

}
